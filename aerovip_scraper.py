import argparse
import csv
import datetime
from datetime import datetime

import urllib
import urllib2
import time
import itertools

from bs4 import BeautifulSoup 

###############################################################################
## Settings
stgs = {
	'max_amount_adults'   : 14,
	'max_amount_children' : 14,
	'max_amount_infants'  : 14,
	'default_num_adults'  : 1,
	'default_num_children': 0,
	'default_num_infants' : 0,

	'num_infants_per_adult'  : 1,
	'num_children_per_adult' : 6
}
###############################################################################
## User Order from commandline
class UserOrder:
	def __init__(self):
		"""Initialize parser and parse given commandline data"""
		self.departure_date = ""
		self.arrival_date   = ""

		self.from_place = 'PXO'
		self.to_place   = 'FNC'

		self.adults   = 1
		self.children = 0
		self.infants  = 0

		self.max_children_without_adult = 1
		self.max_children_per_adult     = 6
		self.max_infants_per_adult      = 1

		self.init_parser()
		self.parse_and_check()

	def init_parser(self):
		"""Initialize Argument parser for AeroVIP scraper"""
		self.parser = argparse.ArgumentParser(description='Scraper for AEROVIP')

		self.parser.add_argument('--from-place', metavar='IATA/ICAO CODE', required=True,
								help = 'The code of point of departure') 
		self.parser.add_argument('--to-place',   metavar='IATA/ICAO CODE', required=True,
								help = 'The code of destination')

		self.parser.add_argument('--departure-date', metavar='DATE', required=True)
		self.parser.add_argument('--arrival-date',   metavar='DATE', required=False)

		self.parser.add_argument('--adults',   type = int, required=True,  nargs=1, default=stgs['default_num_adults']   )
		self.parser.add_argument('--children', type = int, required=False, nargs=1, default=stgs['default_num_children'] )
		self.parser.add_argument('--infants',  type = int, required=False, nargs=1, default=stgs['default_num_infants']  )

	def get_iata_code(self, code):
		"""Chechks given code(should be IATA or ICAO code for 'Porto Santo' or 'Funchal'. Standardize it to IATA"""
		iata_code = ""
		if code.upper() in ['PXO', 'LPPS']:
			iata_code = 'PXO'
		elif code.upper() in ['FNC', 'LPFU']:
			iata_code = 'FNC'
		else:
			raise Exception("Incorrect code for airport. Check value for 'from-place' and 'to-place'. Should be one of: PXO, FNC, LPPS, LPFU")

		return iata_code

	def parse_and_check(self):
		"""Parse and check data received from commandline"""
		args = self.parser.parse_args()

		self.from_place = self.get_iata_code( args.from_place )
		self.to_place   = self.get_iata_code( args.to_place   )
	
		if self.from_place == self.to_place:
			raise Exception("'from-place' and 'to-place' are the same. Specify different airport")

		#date format YYYY-MM-DD
		self.departure_date = datetime.strptime(args.departure_date, '%Y-%m-%d')

		if self.departure_date < datetime.today():
			raise Exception("Your flight is overdue", "Choose another day")

		if args.arrival_date:
			self.arrival_date   = datetime.strptime(args.arrival_date,   '%Y-%m-%d')
			if self.arrival_date < self.departure_date:
				raise Exception("Incorrect arrival date. Choose day that's later than departure date")
			self.arrival_date = self.arrival_date.strftime("%Y-%m-%d")

		self.departure_date = self.departure_date.strftime("%Y-%m-%d")

		#Question: How many children/infants is possible per one adult?
		self.adults = int(args.adults[0])
		if self.adults < 0 or self.adults > stgs['max_amount_adults']:
			raise Exception("Incorrect amount of adults")

		if args.children:
			self.children = int(args.children[0])
			if self.children < 0 or self.children > stgs['max_amount_children']:
				raise Exception("Incorrect amount of children")

		if args.infants:
			self.infants = int(args.infants[0])
			if self.infants < 0 or self.infants > stgs['max_amount_infants']:
				raise Exception("Incorrect amount of infants")

	def get_full_airport_name(self, iata_code):
		if iata_code == 'PXO':
			return "Porto Santo - Portugal (PXO)"
		elif iata_code == 'FNC':
			return "Funchal - Portugal (FNC)"
		else:
			raise( "Unsupported iata code")

	def print_order(self):
		print "*" * 30 + " === ORDER INFO === " + "*" * 30
		print "* From:\t\t"    + self.get_full_airport_name(self.from_place)
		print "* To:\t\t"      + self.get_full_airport_name(self.to_place)
		print "* Departure:\t" + self.departure_date
		print "* Arrival:\t"   + ( self.arrival_date if not self.arrival_date == "" else "<Not required>" )
		print "* Adults:\t",     self.adults
		print "* Children:\t",   self.children
		print "* Infants:\t",    self.infants
		print "*" * 80

###############################################################################
class ElementDebugger:
	def show_inputs(beautiful_html):
		inputs = beautiful_html.findAll('input')
		print "Inputs:"
		for item in inputs:
			print "\t Name-value: '" + item.get('name') + "' - '" + item.get('value') + "'"

	def show_selects(beautiful_html):
		selects = beautiful_html.findAll('select')
		print "Selects:"
		for item in selects:
			print "\tName: " + item.get('name')
			for value in item.stripped_strings:
				print "\t\t" + value

	def show_forms(beautiful_html):
		forms = beautiful_html.findAll('form')
		print "Forms:"
		for item in forms:
			print "\t Name-action: '" + item.get('name') + "' - '" + item.get('action') +"'"

	def show_all(html):
		debug_show_forms(html)
		debug_show_inputs(html)
		debug_show_selects(html)
###############################################################################
##
class AeroVipScraper:
	def __init__(self, order):
		self.data = []
		self.url  = "http://reservas.aerovip.pt/"
		self.aerovip_action = [
			'index.php?s=1&p=1', 
			'index.php?s=1&p=2',
			'index.php?s=1',
			'index.php?s=2',
			'index.php?s=3',
			'index.php?s=4'
		]
		self.cookie = ""
		self.current_html = ""
		self.available_flights = {}
		self.user_order = order

		self.is_from_PXO = self.user_order.from_place == 'PXO' # is from Porto Santo(PXO) to Funchal(FNC)
		self.is_oneway   = self.user_order.arrival_date == "" 
		self.available_combinations = []

	def fill_available_flights(self, select_id):
		if select_id == 'voo_ida':
			direction = 'there'
		elif select_id == 'voo_volta':
			direction = 'back'
		else:
			raise Exception("incorrect value for select_id, should be 'voo_ida' or 'voo_volta'")

		self.available_flights[direction] = {}
		for option in self.current_html.find('select', {'id' : select_id}).findChildren():
			value = option.get('value')
			str1, available_sits = option.text.split(" - Lugares Livres: ")
			flight_time, flight_number = str1.split(" - ")

			if int(available_sits) > self.user_order.adults + self.user_order.children:
				self.available_flights[direction][value] = {
					'time'      : flight_time,
					'number'    : flight_number,
					'sits'      : available_sits
				}
	
	def scrape(self):
		self.make_quotes()

	def build_search_request(self, action, data=""):
		if debug_mode:
			print '-' * 79
			print "| url-action: '" + self.url + action + "'"
			print "| cookie: '" + self.cookie + "'"
			print "| data: '" + data + "'"

		req = urllib2.Request( self.url + action, data)
		resp = ""
		if self.cookie == "":
			resp = urllib2.urlopen(req)
			self.cookie = resp.headers.get('Set-Cookie')
		else:
			req.add_header('cookie', self.cookie)
			resp = urllib2.urlopen(req)

		html  = resp.read()
		if debug_mode:
			print "| resp-len:", len(html)
			print '-' * 79
		return html

	def stage_1(self):
		response = self.build_search_request(self.aerovip_action[0])
		self.current_html = BeautifulSoup(response)

		inputs = self.current_html.findAll('input')
		forms = self.current_html.findAll('form')
		action = forms[0].get('action')
		name   = forms[0].get('name')

		if forms[0].get('name') == 'form3' and forms[0].get('action') == self.aerovip_action[1] and inputs[0].get('name') == 'procurar':
			print " + Good page, let's try next one..."
		else:
			raise Exception("something gets wrong on stage 1 - unexpected content")

	def stage_2(self):
		data = urllib.urlencode({
			'procurar' : 'Seguinte'
		})

		response = self.build_search_request(self.aerovip_action[1], data)
		self.current_html = BeautifulSoup(response)

		inputs = self.current_html.findAll('input')
		forms = self.current_html.findAll('form')
		if len(forms) == 3 and forms[2].get('action') == self.aerovip_action[2] and len(self.current_html.findAll(text='PREMIUM')) == 3:
			print " + Another good page, let's try next one..."
		else:
			raise Exception("something gets wrong on stage 2 - unexpected content")

	def stage_3(self):
		data = urllib.urlencode({
			'procurar' : 'Aceitar'
		})

		response = self.build_search_request(self.aerovip_action[2], data)
		self.current_html = BeautifulSoup(response)

		forms = self.current_html.findAll('form')
		if forms[0].get('action') == self.aerovip_action[3]:
			print " + yet another good page, let's try next one..."
		else:
			raise Exception("something gets wrong on stage 3 - unexpected content")

	def stage_4(self):
		data = urllib.urlencode({
			'tipovoo' : 'ida' if self.is_oneway else 'idavolta',
			'partida' : 1 if self.is_from_PXO else 3,
			'chegada' : 3 if self.is_from_PXO else 1,
			'ida'     : self.user_order.departure_date,
			'volta'   : self.user_order.arrival_date,
			'procurar': 'Procurar Voo'
		})

		response = self.build_search_request(self.aerovip_action[3], data)
		self.current_html = BeautifulSoup(response)

		self.fill_available_flights('voo_ida')
		if not self.is_oneway:
			self.fill_available_flights('voo_volta')

	def stage_5(self, flight_there, flight_back=""):
		data = urllib.urlencode({
			'voo_ida'   : flight_there,
			'voo_volta' : flight_back,
			'Reservar'  : 'Selecionar'
		})

		response = self.build_search_request(self.aerovip_action[4], data)
		self.current_html = BeautifulSoup(response)

	def stage_6(self, N, tarifa=""):
		data = urllib.urlencode({
			'n'        : N,
			'adultos'  : self.user_order.adults,
			'criancas' : self.user_order.children,
			'bebes'    : self.user_order.infants,
			'tarifa'   : 'premium' if tarifa == "" else tarifa,
			'R2'       : 'Selecionar '
		})

		response = self.build_search_request(self.aerovip_action[5], data)
		self.current_html = BeautifulSoup(response)

		# print self.current_html.prettify()
		#Return cost of flight
		strong_cost = self.current_html.find('strong', text='Custo total:')
		print "s: ", strong_cost
		if strong_cost:
			return strong_cost.parent.text.split(' ')[2]
		else:
			return 0

	def make_quotes(self):
		self.stage_1()
		self.stage_2()
		self.stage_3()
		self.stage_4()

		print "There:"
		for flight_value in self.available_flights['there']:
			flight = self.available_flights['there'][flight_value]
			print flight['time'] + " - " + flight['number'] + " - " + flight['sits']

		if not self.is_oneway:
			self.available_combinations = itertools.product(self.available_flights['there'].keys(), self.available_flights['back'].keys())
			print "Back:"
			for flight_value in self.available_flights['back']:
				flight = self.available_flights['back'][flight_value]
				print flight['time'] + " - " + flight['number'] + " - " + flight['sits']
		else:
			self.available_combinations = itertools.product(self.available_flights['there'].keys(), [""])

		# print "Here:"
		# if self.is_oneway:
		# 	flights_with_cost = []
		# 	print "*" * 80
		# 	for value in self.available_flights['there']:
		# 		self.stage_5(value)
		# 		cost = self.stage_6(self.available_flights['there'][value]['sits'])
		# 		flight = {
		# 			'from'       : self.user_order.from_place,
		# 			'to'         : self.user_order.to_place,
		# 		    'there'      : self.available_flights['there'][value]['number'],
		# 		    'there_time' : self.available_flights['there'][value]['time'],
		# 			'back'       : "<Not required>",
		# 			'back_time'  : "<Not required>",
		# 			'total_cost' : cost
		# 		}
		# 		flights_with_cost.append(flight)
		# 		self.print_flight_info(flight)
		# else:
		# 	flights_with_cost = []
		# 	# We're going to check prices for all available combinations
		# 	print '*' * 80
		# 	for item in cartesian_flights:
		# 		self.stage_5(item[0], item[1])
		# 		cost = self.stage_6(self.available_flights['there'][item[0]]['sits'])
		# 		flight = {
		# 			'from'       : self.user_order.from_place,
		# 			'to'         : self.user_order.to_place,
		# 			'there'      : self.available_flights['there'][item[0]]['number'],
		# 			'there_time' : self.available_flights['there'][item[0]]['time'],
		# 			'back'       : self.available_flights['back'][item[1]]['number'],
		# 			'back_time'  : self.available_flights['back'][item[1]]['time'],
		# 			'total_cost' : cost
		# 		}
		# 		flights_with_cost.append(flight)
		# 		self.print_flight_info(flight)

	def print_flight_info(self, flight):
		info  = flight['from'] + "->" + flight['to'] + ": "
		info += flight['there'] + "(" + flight['there_time'] + ") - "
		if not self.is_oneway:
			info += flight['back'] + "(" + flight['back_time'] + ": "
		info += "total cost: " + flight['total_cost']
		print info

	def get_cost_for_choosen_flight(self, tarifa, flight_there, flight_back=""):
		self.stage_1()
		self.stage_1()
		self.stage_2()
		self.stage_3()
		self.stage_4()
		self.stage_5(flight_there, flight_back)
		cost = self.stage_6(self.available_flights['there'][flight_there]['sits'], tarifa)
		return cost

###############################################################################
## Main section

try:
	debug_mode = False
	my_order = UserOrder()
	my_order.print_order()

	my_scraper = AeroVipScraper(my_order)
	my_scraper.scrape()

	tarifas = ['premium', 'basic'] #it's case sensetive
	all_available_combinations = itertools.product(my_scraper.available_combinations, tarifas)

	counter = 5 #debug only
	for row in all_available_combinations:
	 	one_task = AeroVipScraper(my_order)
	 	cost = one_task.get_cost_for_choosen_flight(row[1], row[0][0], row[0][1]) 
		print "Cost: ", cost, "Tarifa: ", row[1], "there: ", row[0][0], "back: ", (row[0][1] if not one_task.is_oneway else "<Not required>")
		counter -= 1
		if counter == 0:
			break

except Exception as exc:
	print "ERROR:", exc.args[0]
	print exc

